"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const Snoowrap = require("snoowrap");
/**
 * This class creates, stores, and retrieves Snoowrap singletons so all services can share the same rate limiter.
 *
 * @export
 * @class SnoowrapManager
 */
class SnoowrapManager {
    /**
     * Creates an instance of SnoowrapManager.
     * @memberof SnoowrapManager
     */
    constructor(creds, config = {}) {
        this._SNOOWRAP_KEY = Symbol.for("pcmrbotjs.reddit.snoowrap");
        this._APPCREDS = {
            userAgent: creds.userAgent,
            clientId: creds.clientId,
            clientSecret: creds.clientSecret
        };
        this._CONFIG = config;
        if (Object.getOwnPropertySymbols(global).indexOf(this._SNOOWRAP_KEY) === -1) {
            let snoowrap = new Snoowrap(creds);
            snoowrap.config(config);
            // Test to see whether the supplied credentials actually work
            try {
                snoowrap.getMe();
            }
            catch (err) {
                // TODO: Differentiate between Reddit service outages and invalid creds
                throw err;
            }
            global[this._SNOOWRAP_KEY] = {};
            global[this._SNOOWRAP_KEY].default = snoowrap;
        }
    }
    /**
     * Retrieves a Snoowrap singleton for the specified user
     * @param {string} [username] - The Reddit username for the specified account. Omitting this will return the "default" user
     * @returns {Snoowrap} The Snoowrap instance for the specified user
     * @throws {Errors.MoleculerError} Throws an error if the user does not exist within the global cache
     */
    getInstance(username) {
        if (!username) {
            return global[this._SNOOWRAP_KEY].default;
        }
        else if (username && global[this._SNOOWRAP_KEY].hasOwnProperty(username.toLowerCase())) {
            return global[this._SNOOWRAP_KEY][username.toLowerCase()];
        }
        else {
            // If there's no key, throw an error. The service should add the new account as this has no broker access
            throw new moleculer_1.Errors.MoleculerError("No account with that name in Snoowrap manager", 511, "USER_NOT_IN_MANAGER");
        }
    }
    /**
     * Adds a new user to the global Snoowrap store.
     * @param {string} username - The username of the account to add. If the user is already added, this will return the existing instance
     * @param {string} refreshToken - The refresh token to use
     * @returns {Snoowrap} Returns the newly added instance
     * @throws Throws an error if Reddit is having issues
     */
    addInstance(username, refreshToken) {
        try {
            return this.getInstance(username.toLowerCase());
        }
        catch (err) {
            if (err.type === "USER_NOT_IN_MANAGER") {
                let snoowrap = new Snoowrap({
                    userAgent: this._APPCREDS.userAgent,
                    clientId: this._APPCREDS.clientId,
                    clientSecret: this._APPCREDS.clientSecret,
                    refreshToken: refreshToken
                });
                snoowrap.config(this._CONFIG);
                // Ensure that the refresh token actually works
                try {
                    snoowrap.getMe();
                }
                catch (err) {
                    // TODO: Handle Reddit service outages
                    throw err;
                }
                global[this._SNOOWRAP_KEY][username.toLowerCase()] = snoowrap;
                return this.getInstance(username.toLowerCase());
            }
            else {
                // If getInstance() throws something other than the only error I put in, then something's wrong
                throw err;
            }
        }
    }
}
exports.default = SnoowrapManager;
