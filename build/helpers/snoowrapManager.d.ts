import * as Snoowrap from "snoowrap";
/**
 * This class creates, stores, and retrieves Snoowrap singletons so all services can share the same rate limiter.
 *
 * @export
 * @class SnoowrapManager
 */
export default class SnoowrapManager {
    private _SNOOWRAP_KEY;
    private _APPCREDS;
    private _CONFIG;
    /**
     * Creates an instance of SnoowrapManager.
     * @memberof SnoowrapManager
     */
    constructor(creds: Snoowrap.SnoowrapOptions, config?: Snoowrap.ConfigOptions);
    /**
     * Retrieves a Snoowrap singleton for the specified user
     * @param {string} [username] - The Reddit username for the specified account. Omitting this will return the "default" user
     * @returns {Snoowrap} The Snoowrap instance for the specified user
     * @throws {Errors.MoleculerError} Throws an error if the user does not exist within the global cache
     */
    getInstance(username?: string): Snoowrap;
    /**
     * Adds a new user to the global Snoowrap store.
     * @param {string} username - The username of the account to add. If the user is already added, this will return the existing instance
     * @param {string} refreshToken - The refresh token to use
     * @returns {Snoowrap} Returns the newly added instance
     * @throws Throws an error if Reddit is having issues
     */
    addInstance(username: string, refreshToken: string): Snoowrap;
}
