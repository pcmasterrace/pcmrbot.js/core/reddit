'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
// Because this is a ServiceSchema instead of a regular ES6 class, I have to use require() instead
const RedditPostService = require('./services/post');
exports.RedditPostService = RedditPostService;
const comment_1 = require('./services/comment');
exports.RedditCommentService = comment_1.default;
const flair_1 = require('./services/flair');
exports.RedditFlairService = flair_1.default;
const modlog_1 = require('./services/modlog');
exports.RedditModlogService = modlog_1.default;
const modmail_1 = require('./services/modmail');
exports.RedditModmailService = modmail_1.default;
const modqueue_1 = require('./services/modqueue');
exports.RedditModqueueService = modqueue_1.default;
const submission_1 = require('./services/submission');
exports.RedditSubmissionService = submission_1.default;
const subreddit_1 = require('./services/subreddit');
exports.RedditSubredditService = subreddit_1.default;
const toolbox_1 = require('./services/toolbox');
exports.RedditToolboxService = toolbox_1.default;
const user_1 = require('./services/user');
exports.RedditUserService = user_1.default;
const wiki_1 = require('./services/wiki');
exports.RedditWikiService = wiki_1.default;
/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker
 */
function registerAllRedditServices(broker) {
    broker.createService(RedditPostService);
    broker.createService(comment_1.default);
    broker.createService(flair_1.default);
    broker.createService(modlog_1.default);
    broker.createService(modmail_1.default);
    broker.createService(modqueue_1.default);
    broker.createService(submission_1.default);
    broker.createService(subreddit_1.default);
    broker.createService(toolbox_1.default);
    broker.createService(user_1.default);
    broker.createService(wiki_1.default);
}
exports.default = registerAllRedditServices;