import { ServiceBroker, ServiceSchema } from "moleculer";
declare const RedditPostService: ServiceSchema;
import RedditCommentService from "./services/comment";
import RedditFlairService from "./services/flair";
import RedditModlogService from "./services/modlog";
import RedditModmailService from "./services/modmail";
import RedditModqueueService from "./services/modqueue";
import RedditSubmissionService from "./services/submission";
import RedditSubredditService from "./services/subreddit";
import RedditToolboxService from "./services/toolbox";
import RedditUserService from "./services/user";
import RedditWikiService from "./services/wiki";
/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker
 */
export default function registerAllRedditServices(broker: ServiceBroker): void;
export { RedditPostService, RedditCommentService, RedditFlairService, RedditModlogService, RedditModmailService, RedditModqueueService, RedditSubmissionService, RedditSubredditService, RedditToolboxService, RedditUserService, RedditWikiService };
