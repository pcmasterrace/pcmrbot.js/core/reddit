import { Context, Service } from "moleculer";
import * as Snoowrap from "snoowrap";
/**
 * This module has utilities to work with the modlog.
 *
 * @module "reddit.modlog"
 * @version 3
 */
export default class RedditModlogService extends Service {
    protected reddit: Snoowrap;
    constructor(broker: any);
    /**
     * Retrieves the moderation log for a subreddit.
     * @function
     * @static
     * @name getModlog
     * @param {string} subreddit - The subreddit to retrieve the modlog from
     * @param {string} [before] - The ModAction to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The ModAction to start retrieving from. All retrieved actions will be **older** than this
     * @param {string[]} [mods] - An array of moderator names
     * @param {string} [type] - The action to retrieve. One of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent`
     * @param {number} [limit=25] - The number of items to retrieve in one call
     * @param {number} [pages=1] - The number of calls to make. The number of returned items is equal to `pages` * `limit`
     * @param {boolean} [fetchAll] - Whether all available actions should be retrieved. Can only be used with `before`
     * @returns An array of mod actions
     * @throws Throws an error when `fetchAll` is defined with `after`, or if `fetchAll` is defined without `before` or `after`
     */
    getModlog(ctx: Context): Promise<Snoowrap.Listing<Snoowrap.ModAction>>;
}
