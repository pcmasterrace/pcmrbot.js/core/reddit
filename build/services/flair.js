'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
const stream_1 = require('stream');
// TypeScript complains unless I do this
const RedditCoreService = require('./mixin');
/**
 * This module provides utilities to work with flairs.
 *
 * @module "reddit.flair"
 * @version 3
 */
class RedditFlairService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.flair',
            version: 3,
            mixins: [RedditCoreService],
            actions: {
                getUser: {
                    name: 'get.user',
                    params: {
                        subreddit: 'string',
                        user: 'string'
                    },
                    handler: this.getUser
                },
                getAll: {
                    name: 'get.all',
                    params: {
                        subreddit: 'string',
                        limit: {
                            type: 'number',
                            optional: true,
                            integer: true,
                            positive: true,
                            max: 1000
                        },
                        after: {
                            type: 'string',
                            optional: true
                        }
                    },
                    handler: this.getAll
                }
            }
        });
    }
    async getUser(ctx) {
        const reddit = ctx.meta.reddit;
        try {
            return await reddit.getSubreddit(ctx.params.subreddit).getUserFlair(ctx.params.user);
        } catch (err) {
            throw new moleculer_1.Errors.MoleculerError('Snoowrap error', err.code, 'CHANGE_THIS', err);
        }
    }
    async getAll(ctx) {
        const reddit = ctx.meta.reddit;
        // The caller should have supplied a stream
        const stream = new stream_1.Readable({
            read() {
            }
        });
        let flairCount = 0;
        // Do it once first outside of the loop so the typing is correctly set, also because the params are different 
        reddit.getSubreddit(ctx.params.subreddit).getUserFlairList({ limit: 1000 }).then(async listing => {
            for (let flair of listing) {
                flairCount++;
                stream.push(JSON.stringify({
                    flair_css_class: flair.flair_css_class,
                    //@ts-ignore (this is a typings bug)
                    name: flair.user.name,
                    flair_text: flair.flair_text
                }));
            }
            while (!listing.isFinished) {
                listing = await listing.fetchMore({
                    amount: 1000,
                    append: false
                });
                for (let flair of listing) {
                    flairCount++;
                    stream.push(JSON.stringify({
                        flair_css_class: flair.flair_css_class,
                        //@ts-ignore (this is a typings bug)
                        name: flair.user.name,
                        flair_text: flair.flair_text
                    }));
                }
            }
        }).then(() => {
            // End stream
            stream.push(null);
        });
        return stream;
    }
}
exports.default = RedditFlairService;