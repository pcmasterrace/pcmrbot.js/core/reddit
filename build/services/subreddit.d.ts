import { Service, Context } from "moleculer";
import * as Snoowrap from "snoowrap";
/**
 * This module exposes various utilities to work with subreddits.
 *
 * @module "reddit.subreddit"
 * @version 3
 */
export default class RedditSubredditService extends Service {
    constructor(broker: any);
    /**
     * Gets the specified index for the given subreddit
     * @function
     * @name getIndex
     * @static
     * @param {string} subreddit - The subreddit to retrieve
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     * @param {string} [sort=hot] - The sort to apply. Acceptable values are `hot`, `new`, `rising`, `controversial`, and `top`
     * @param {string} [time] - The time range to get. Acceptable values are `hour`, `day`, `week`, `month`, `year`, `all`. Defaults to `hour`. Usable only with `controversial` and `top`
     * @returns Returns the Reddit API response
     */
    getIndex(ctx: Context): Promise<Snoowrap.Listing<Snoowrap.Submission>>;
    /**
     * Retrieves new comments from the specified subreddit.
     * @function
     * @static
     * @name getComments
     * @param {string} subreddit - The subreddit to retrieve
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     */
    getComments(ctx: Context): Promise<Snoowrap.Listing<Snoowrap.Comment>>;
    /**
     * Gets the available link flair templates for the given subreddit
     * @function
     * @name getLinkFlairs
     * @static
     * @param {string} subreddit - The subreddit to look up
     * @returns Returns the Reddit API response
     */
    getLinkFlairs(ctx: Context): Promise<any>;
}
