import { Context, Service } from "moleculer";
import * as Snoowrap from "snoowrap";
/**
 * This module exposes various utilities to work with New Modmail
 *
 * @module "reddit.modmail"
 * @version 3
 */
export default class ModmailService extends Service {
    protected reddit: Snoowrap;
    constructor(broker: any);
    /**
     * Retrieves all of the messages and stuff in a conversation.
     * @function
     * @static
     * @name getConversation
     * @param {string} conversationId - The conversation ID.
     * @returns {object} The conversation as returned by Reddit.
     */
    getConversation(ctx: Context): Promise<ModmailConversation>;
    /**
     * Retrieve the current list of modmails.
     * @function
     * @static
     * @name getConversations
     * @param {string} [after=new] - Retrieves modmails older than the supplied ID
     * @param {string} [entity=all] - Comma-delimited list of subreddit names
     * @param {number} [limit=25] - Number of modmails to retrieve
     * @param {string} [sort=new] - One of the following: `recent`, `new`, `mod`, `unread`
     * @param {string} [state=all] - One of the following: `new`, `inprogress`, `mod`, `notifications`, `archived`, `highlighted`, `all`
     * @returns {object} The results of the API request.
     */
    getConversations(ctx: Context): Promise<any>;
    /**
     * Creates a new modmail conversation.
     * @function
     * @static
     * @name createNewConversation
     * @param {string} body - The Markdown body of the message
     * @param {boolean} isAuthorHidden - Whether the message should be sent as a user or as the subreddit
     * @param {string} srName - The subreddit from which to send the message
     * @param {string} subject - The message subject
     * @param {string} to - The recipient of the message
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API response, probably the base36 ID of the conversation.
     */
    createNewConversation(ctx: Context): Promise<any>;
    /**
     * Posts a new message to a specified modmail conversation.
     * @function
     * @static
     * @name postNewReply
     * @param {string} conversationId - The base36 conversation ID
     * @param {string} body - The Markdown body of the message
     * @param {boolean} [isAuthorHidden=false] - Boolean representing whether message should be sent as a user or as subreddit
     * @param {boolean} [isInternal=false] - Boolean representing whether the message is an internal message
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API response to this.
     */
    postNewReply(ctx: Context): Promise<any>;
    /**
     * Archives a modmail conversation
     * @function
     * @static
     * @name archive
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    archive(ctx: Context): Promise<any>;
    /**
     * Unarchives a modmail conversation
     * @function
     * @static
     * @name unarchive
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    unarchive(ctx: Context): Promise<any>;
    /**
     * Highlights a modmail conversation
     * @function
     * @static
     * @name highlight
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    highlight(ctx: Context): Promise<any>;
    /**
     * Removes highlight from a modmail conversation
     * @function
     * @static
     * @name unhighlight
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    unhighlight(ctx: Context): Promise<any>;
    /**
     * Mutes a modmail conversation
     * @function
     * @static
     * @name mute
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    mute(ctx: Context): Promise<any>;
    /**
     * Unmutes a modmail conversation
     * @function
     * @static
     * @name unmute
     * @param {string} conversationId - The base36 ID of the specified conversation
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    unmute(ctx: Context): Promise<any>;
    /**
     * Marks a modmail conversation, or a set of modmail conversations, read.
     * @function
     * @static
     * @name markRead
     * @param {string} conversationIds - A comma separated list of base36 conversation IDs
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    markRead(ctx: Context): Promise<any>;
    /**
     * Marks a modmail conversation, or a set of modmail conversations, unread.
     *
     * @function
     * @static
     * @name markUnread
     * @param {string} conversationIds - A comma separated list of base36 conversation IDs
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    markUnread(ctx: Context): Promise<any>;
    /**
     * Gets the current unread modmail count.
     * @function
     * @static
     * @name getUnreadCount
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    getUnreadCount(ctx: Context): Promise<any>;
    /**
     * Gets a list of subreddits that the current authenticated user has access to
     * @function
     * @static
     * @name getSubreddits
     * @param {string} [redditName=Default user] - The username of the user to act as
     * @param {string} [slackId=Default user] - The slack ID of the user to act as
     * @returns {object} The API results.
     */
    getSubreddits(ctx: Context): Promise<any>;
}
interface ModmailConversation {
    conversation: ModmailConversationEntry[];
    info: {
        id: string;
        subreddit: string;
        subredditId: string;
        isAuto: boolean;
        isHighlighted: boolean;
        subject: string;
    };
}
interface ModmailConversationEntry {
    type: "message" | "modAction";
    id: string;
    author: {
        name: string;
        id: string | number;
        isOp: boolean;
        isMod: boolean;
        isAdmin: boolean;
        isHidden: boolean;
        created?: string;
        muteStatus?: {
            isMuted: boolean;
            expiresOn: string;
            muteReason: string;
        };
        banStatus?: {
            isBanned: boolean;
            isPermanent: boolean;
            expiresOn: string;
            banReason: string;
        };
        isSuspended?: boolean;
        isShadowbanned?: boolean;
        recent?: {
            comments?: {
                [name: string]: {
                    comment: string;
                    date: string;
                    permalink: string;
                    title: string;
                };
            };
            posts?: {
                [name: string]: {
                    date: string;
                    permalink: string;
                    title: string;
                };
            };
            conversations?: {
                [name: string]: {
                    date: string;
                    id: string;
                    subject: string;
                };
            };
        };
    };
    date: string;
    isInternal?: boolean;
    message?: string;
    actionTypeId?: number;
}
export {};
