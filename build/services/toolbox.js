'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// TypeScript complains unless I do this
const RedditCoreService = require('./mixin');
/**
 * This module exposes several utilities to work with the Moderator Toolbox for Reddit configuration.
 *
 * @module "reddit.toolbox"
 * @version 3
 */
class RedditToolboxService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.toolbox',
            version: 3,
            mixins: [RedditCoreService],
            dependencies: [{
                    name: 'reddit.wiki',
                    version: 3
                }],
            actions: {
                getConfig: {
                    name: 'getConfig',
                    cache: true,
                    params: { subreddit: 'string' },
                    handler: this.getConfig
                },
                renderRemovalMessage: {
                    name: 'renderRemovalMessage',
                    params: {
                        subreddit: 'string',
                        reasons: {
                            type: 'array',
                            items: 'string'
                        },
                        useHeader: {
                            type: 'boolean',
                            optional: true
                        },
                        useFooter: {
                            type: 'boolean',
                            optional: true
                        },
                        author: {
                            type: 'string',
                            optional: true
                        },
                        kind: {
                            type: 'string',
                            optional: true
                        },
                        mod: {
                            type: 'string',
                            optional: true
                        },
                        title: {
                            type: 'string',
                            optional: true
                        },
                        url: {
                            type: 'string',
                            optional: true
                        },
                        domain: {
                            type: 'string',
                            optional: true
                        },
                        link: {
                            type: 'string',
                            optional: true
                        }
                    },
                    handler: this.renderRemovalMessage
                }
            }
        });
    }
    /**
     * Retrieves and parses the Toolbox configuration.
     * @function
     * @static
     * @name getConfig
     * @param {string} subreddit - The subreddit from which to retrieve the Toolbox config
     * @returns {object} Returns the Toolbox configuration
     * @throws Throws if Reddit experiences service interruptions
     */
    async getConfig(ctx) {
        let config = await ctx.call('v2.reddit.wiki.getPage', {
            subreddit: ctx.params.subreddit,
            page: 'toolbox'
        }, {
            meta: {
                serviceType: ctx.meta.serviceType || undefined,
                userId: ctx.meta.userId || undefined
            }
        });
        config = JSON.parse(config.content_md);
        // Async decode these because reasons
        let macros = config.modMacros.map(async macro => {
            macro.text = unescape(macro.text);
            return macro;
        });
        let reasons = config.removalReasons.reasons.map(async reason => {
            reason.text = unescape(reason.text);
            return reason;
        });
        let escaped = await Promise.all([
            Promise.all(macros),
            Promise.all(reasons)
        ]);
        config.macros = escaped[0];
        config.removalReasons.header = unescape(config.removalReasons.header);
        config.removalReasons.reasons = escaped[1];
        config.removalReasons.footer = unescape(config.removalReasons.footer);
        return config;
    }
    /**
     * Renders out a removal reason using the Toolbox configuration.
     * @function
     * @static
     * @name renderRemovalMessage
     * @param {string} subreddit - The subreddit to get the removal reasons from. Also used for the `{subreddit}` token
     * @param {string[]} reasons - A string array of removal reasons. This will render out the first rule that the string matches
     * @param {boolean} [useHeader=true] - Whether to use the header for the removal message
     * @param {boolean} [useFooter=true] - Whether to use the footer for the removal message
     * @param {string} [author] - The author of the post being removed. Used for the `{author}` token
     * @param {string} [kind] - The kind of post. Must be `submission` or `comment`. Used for the `{kind}` token
     * @param {string} [mod] - The name of the mod removing the past. Used for the `{mod}` token
     * @param {string} [title] - The title of the post being removed. Used for the `{title}` token
     * @param {string} [url] - The URL to the post being removed. Used for the `{url}` token
     * @param {string} [domain] - The domain of the removed submission. Used for the `{domain}` token
     * @param {string} [link] - The destination link of the removed submission. Used for the `{link}` token
     * @returns {string} The rendered removal reason
     */
    async renderRemovalMessage(ctx) {
        const tbConfig = await ctx.call('v2.reddit.toolbox.getConfig', { subreddit: ctx.params.subreddit }, {
            meta: {
                serviceType: ctx.meta.serviceType || undefined,
                userId: ctx.meta.userId || undefined
            }
        });
        const reasons = tbConfig.removalReasons.reasons;
        let message = '';
        if (ctx.params.useHeader || ctx.params.useHeader === undefined)
            message += tbConfig.removalReasons.header + '\n\n';
        for (let reasonTitle of ctx.params.reasons) {
            let matchingReasons = reasons.filter(reason => {
                return reason.title.toLowerCase().includes(reasonTitle.toLowerCase());
            });
            message += matchingReasons[0].text + '\n';
        }
        if (ctx.params.useFooter || ctx.params.useFooter === undefined)
            message += '\n' + tbConfig.removalReasons.footer;
        message = message.replace(/\{subreddit\}/g, ctx.params.subreddit);
        message = message.replace(/\{author\}/g, ctx.params.author);
        message = message.replace(/\{kind\}/g, ctx.params.kind);
        message = message.replace(/\{mod\}/g, ctx.params.mod);
        message = message.replace(/\{title\}/g, ctx.params.title);
        message = message.replace(/\{url\}/g, ctx.params.url);
        message = message.replace(/\{domain\}/g, ctx.params.domain);
        message = message.replace(/\{link\}/g, ctx.params.link);
        return message;
    }
}
exports.default = RedditToolboxService;