import { Context, Service } from "moleculer";
/**
 * This module provides utilities to work with comments.
 * All utilities available within `reddit.post` are present in this service.
 *
 * @module "reddit.comment"
 * @version 3
 */
export default class RedditCommentService extends Service {
    constructor(broker: any);
    /**
     * Replies to the specified submission or comment.
     * @function
     * @static
     * @name reply
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @param {string} text - The message text to reply with
     * @returns {object} API response from Reddit
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment or a submission
     */
    reply(ctx: Context): Promise<any>;
    /**
     * Distinguishes and stickies the specified comment. This only works for top level comments made by the account being used.
     * @function
     * @static
     * @name sticky
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified does not belong to the account specified
     */
    sticky(ctx: Context): Promise<boolean>;
    /**
     * Unstickies the specified comment and optionally undistinguishes it. This only works for top level comments made by the account being used.
     * @function
     * @static
     * @name sticky
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @param {boolean} [undistinguish=true] - Whether the post should also be undistinguished
     * @returns {object} API response from Reddit
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified does not belong to the account specified
     */
    unsticky(ctx: Context): Promise<boolean>;
}
