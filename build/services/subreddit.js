'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
const RedditCoreService = require('./mixin');
/**
 * This module exposes various utilities to work with subreddits.
 *
 * @module "reddit.subreddit"
 * @version 3
 */
class RedditSubredditService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.subreddit',
            version: 3,
            mixins: [RedditCoreService],
            actions: {
                getIndex: {
                    name: 'getIndex',
                    params: {
                        subreddit: 'string',
                        before: {
                            type: 'string',
                            optional: true
                        },
                        after: {
                            type: 'string',
                            optional: true
                        },
                        limit: {
                            type: 'number',
                            integer: true,
                            min: 1,
                            max: 100,
                            optional: true
                        },
                        sort: {
                            type: 'enum',
                            optional: true,
                            values: [
                                'hot',
                                'new',
                                'rising',
                                'controversial',
                                'top'
                            ]
                        },
                        time: {
                            type: 'enum',
                            optional: true,
                            values: [
                                'hour',
                                'day',
                                'week',
                                'month',
                                'year',
                                'all'
                            ]
                        }
                    },
                    handler: this.getIndex
                },
                getComments: {
                    name: 'getComments',
                    params: {
                        subreddit: 'string',
                        before: {
                            type: 'string',
                            optional: true
                        },
                        after: {
                            type: 'string',
                            optional: true
                        },
                        limit: {
                            type: 'number',
                            integer: true,
                            min: 1,
                            max: 100,
                            optional: true
                        }
                    },
                    handler: this.getComments
                },
                getLinkFlairs: {
                    name: 'getLinkFlairs',
                    cache: { ttl: 7200 },
                    params: { subreddit: 'string' },
                    handler: this.getLinkFlairs
                }
            }
        });
    }
    /**
     * Gets the specified index for the given subreddit
     * @function
     * @name getIndex
     * @static
     * @param {string} subreddit - The subreddit to retrieve
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     * @param {string} [sort=hot] - The sort to apply. Acceptable values are `hot`, `new`, `rising`, `controversial`, and `top`
     * @param {string} [time] - The time range to get. Acceptable values are `hour`, `day`, `week`, `month`, `year`, `all`. Defaults to `hour`. Usable only with `controversial` and `top`
     * @returns Returns the Reddit API response
     */
    async getIndex(ctx) {
        const reddit = ctx.meta.reddit;
        const subreddit = reddit.getSubreddit(ctx.params.subreddit);
        let params = {
            before: ctx.params.before || undefined,
            after: ctx.params.after || undefined,
            limit: ctx.params.limit || undefined
        };
        switch (ctx.params.sort) {
        case 'new':
            return await subreddit.getNew(params);
        case 'rising':
            return await subreddit.getRising(params);
        case 'controversial':
            Object.assign(params, { time: ctx.params.time || 'hour' });
            return await subreddit.getControversial(params);
        case 'top':
            Object.assign(params, { time: ctx.params.time || 'hour' });
            return await subreddit.getTop(params);
        case 'hot':
        default:
            return await subreddit.getHot(params);
        }
    }
    /**
     * Retrieves new comments from the specified subreddit.
     * @function
     * @static
     * @name getComments
     * @param {string} subreddit - The subreddit to retrieve
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     */
    async getComments(ctx) {
        const reddit = ctx.meta.reddit;
        return await reddit.getSubreddit(ctx.params.subreddit).getNewComments({
            before: ctx.params.before || undefined,
            after: ctx.params.after || undefined,
            limit: ctx.params.limit || undefined
        });
    }
    /**
     * Gets the available link flair templates for the given subreddit
     * @function
     * @name getLinkFlairs
     * @static
     * @param {string} subreddit - The subreddit to look up
     * @returns Returns the Reddit API response
     */
    async getLinkFlairs(ctx) {
        const reddit = ctx.meta.reddit;
        return await reddit.oauthRequest({
            uri: `/r/${ ctx.params.subreddit }/api/link_flair_v2`,
            method: 'get'
        });
    }
}
exports.default = RedditSubredditService;