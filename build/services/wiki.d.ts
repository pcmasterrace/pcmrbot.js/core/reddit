import { Service, Context } from "moleculer";
/**
 * This module exposes several utilities to work with subreddit wikis.
 *
 * @module "reddit.wiki"
 * @version 3
 */
export default class RedditWikiService extends Service {
    constructor(broker: any);
    /**
     * Retrieves a wiki page from the specified subreddit
     * @function
     * @static
     * @name getPage
     * @param {string} subreddit - The subreddit to retrieve the page from
     * @param {string} page - The wiki page to retrieve
     * @returns {object} The API response from Reddit
     * @throws {Errors.MoleculerError} Throws an error if Reddit encounters any service related issues
     */
    getPage(ctx: Context): Promise<any>;
    /**
     * Creates or updates the content of a wiki page
     * @function
     * @static
     * @name setPage/createPage
     * @param {string} subreddit - The subreddit to edit the page on
     * @param {string} page - The wiki page to edit/create
     * @param {string} text - The updated contents of the wiki page
     * @param {string} [reason] - The edit reason for the page
     * @returns {object} The API response from Reddit
     * @throws {Errors.MoleculerError} Throws an error if Reddit encounters any service related issues
     */
    setPage(ctx: Context): Promise<any>;
}
