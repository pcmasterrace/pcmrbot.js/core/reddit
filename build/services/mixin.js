'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
const stream_1 = require('stream');
const snoowrapManager_1 = require('../helpers/snoowrapManager');
const secret_box_1 = require('@pcmrbotjs/secret-box');
/**
 * Base Reddit service mixin. This module initializes the service-wide Snoowrap object.
 *
 * @module "reddit.core"
 * @version 3
 */
module.exports = {
    name: 'reddit.core',
    version: 3,
    settings: {
        $secureSettings: [
            'reddit.clientSecret',
            'reddit.refreshToken',
            'tokenSecret'
        ],
        reddit: {
            userAgent: process.env.REDDIT_OAUTH_USERAGENT,
            clientId: process.env.REDDIT_OAUTH_CLIENTID,
            clientSecret: process.env.REDDIT_OAUTH_CLIENTSECRET,
            refreshToken: process.env.REDDIT_OAUTH_REFRESHTOKEN
        },
        tokenSecret: process.env.REDDIT_TOKEN_SECRET || undefined
    },
    hooks: {
        before: { '*': 'retrieveUserRefreshToken' },
        after: { '*': 'stringifyForTransport' }
    },
    methods: {
        async retrieveUserRefreshToken(ctx) {
            // The reason why we aren't reusing existing Snoowrap objects on ctx.meta is because I don't know how this works with network calls
            if (!ctx.meta.serviceType && !ctx.meta.userId) {
                // `Get` the default Reddit instance
                ctx.meta.reddit = this.accountManager.getInstance();
                return;
            } else if (!ctx.meta.serviceType || !ctx.meta.userId) {
                // Throw an error if only one piece of information is present. We need both.
                throw new moleculer_1.Errors.MoleculerClientError(`Action called specifying user refresh token, but call is missing the "${ !ctx.meta.serviceType ? 'serviceType' : 'userId' }" meta parameter`, 400, 'MISSING_META_PARAMETER', ctx.meta);
            }
            let userInfo;
            try {
                userInfo = await this.broker.call('v3.accounts.getAssociatedAccount', {
                    serviceType: ctx.meta.serviceType,
                    userId: ctx.meta.userId,
                    getAccountType: 'reddit'
                });
            } catch (err) {
                // TODO: Handle the bare minimum of errors now, add more handling later
                if (err.type === 'SERVICE_NOT_FOUND' || err.type === 'SERVICE_NOT_AVAILABLE' || err.type === 'REQUEST_TIMEOUT') {
                    // Why did I not think of this before writing all of the single user mode stuff
                    throw new moleculer_1.Errors.MoleculerRetryableError(`Cannot contact Accounts service. Multi user functionality is disabled at this time`, 503, 'MULTI_USER_DISABLED');
                } else if (err.type === 'NO_ASSOCIATED_ACCOUNT') {
                    // TODO: No linked account, start linkage procedure
                    throw new moleculer_1.Errors.MoleculerError('No associated account', 403, 'NO_ASSOCIATED_ACCOUNT');
                } else {
                    // Rethrow for now
                    throw err;
                }
            }
            // Get Snoowrap instance, if present
            let snoowrap;
            try {
                snoowrap = this.accountManager.getInstance(userInfo.userId);
            } catch (err) {
                if (err.type === 'USER_NOT_IN_MANAGER') {
                    if (!userInfo.token) {
                        throw new moleculer_1.Errors.MoleculerError('No associated account', 403, 'NO_ASSOCIATED_ACCOUNT');
                    }
                    let refreshToken = this.cipher.decrypt(userInfo.token);
                    snoowrap = this.accountManager.addInstance(userInfo.userId, refreshToken);
                } else {
                    // TODO: Likely just a Reddit service outage, handle later
                    throw err;
                }
            }
            ctx.meta.reddit = snoowrap;
        },
        stringifyForTransport(ctx, res) {
            // DON'T stringify streams, for obvious reasons
            if (res instanceof stream_1.Readable) {
                return res;
            } else {
                return JSON.parse(JSON.stringify(res || {}));
            }
        }
    },
    created() {
        this.accountManager = new snoowrapManager_1.default({
            userAgent: this.settings.reddit.userAgent,
            clientId: this.settings.reddit.clientId,
            clientSecret: this.settings.reddit.clientSecret,
            refreshToken: this.settings.reddit.refreshToken
        });
        if (this.settings.tokenSecret) {
            this.cipher = new secret_box_1.Cipher(this.settings.tokenSecret);
        } else {
            throw new moleculer_1.Errors.MoleculerError('User store decryption key not defined!', 500, 'DECRYPTION_KEY_NOT_DEFINED');
        }
    }
};