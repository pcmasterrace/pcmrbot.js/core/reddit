'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// This needs to be a require() to avoid type errors for some reason
const RedditCoreService = require('./mixin');
/**
 * This module has utilities to work with the modlog.
 *
 * @module "reddit.modlog"
 * @version 3
 */
class RedditModlogService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.modlog',
            version: 3,
            mixins: [RedditCoreService],
            actions: {
                getModlog: {
                    name: 'getModlog',
                    params: {
                        subreddit: 'string',
                        before: {
                            type: 'string',
                            optional: true
                        },
                        after: {
                            type: 'string',
                            optional: true
                        },
                        mods: {
                            type: 'array',
                            items: 'string',
                            optional: true
                        },
                        type: {
                            type: 'string',
                            optional: true
                        },
                        limit: {
                            type: 'number',
                            min: 1,
                            max: 500,
                            optional: true
                        },
                        pages: {
                            type: 'number',
                            min: 1,
                            max: 10,
                            optional: true
                        },
                        fetchAll: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.getModlog
                }
            }
        });
    }
    /**
     * Retrieves the moderation log for a subreddit.
     * @function
     * @static
     * @name getModlog
     * @param {string} subreddit - The subreddit to retrieve the modlog from
     * @param {string} [before] - The ModAction to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The ModAction to start retrieving from. All retrieved actions will be **older** than this
     * @param {string[]} [mods] - An array of moderator names
     * @param {string} [type] - The action to retrieve. One of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent`
     * @param {number} [limit=25] - The number of items to retrieve in one call
     * @param {number} [pages=1] - The number of calls to make. The number of returned items is equal to `pages` * `limit`
     * @param {boolean} [fetchAll] - Whether all available actions should be retrieved. Can only be used with `before`
     * @returns An array of mod actions
     * @throws Throws an error when `fetchAll` is defined with `after`, or if `fetchAll` is defined without `before` or `after`
     */
    async getModlog(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.fetchAll && ctx.params.after !== undefined || ctx.params.fetchAll && ctx.params.after === undefined && ctx.params.before === undefined) {
            throw new moleculer_1.Errors.MoleculerClientError('Parameters as supplied will enter an infinite loop', 401, 'INVALID_PARAMS_ERROR');
        }
        if (ctx.params.pages === undefined) {
            ctx.params.pages = 1;
        }
        let modlog = await reddit.getSubreddit(ctx.params.subreddit).getModerationLog({
            mods: ctx.params.mods || undefined,
            type: ctx.params.type || undefined,
            before: ctx.params.before || undefined,
            after: ctx.params.after || undefined,
            limit: ctx.params.limit || 25
        });
        if (ctx.params.fetchAll) {
            modlog = await modlog.fetchAll();
        } else if (ctx.params.pages > 1) {
            for (let i = 1; i < ctx.params.pages; i++) {
                modlog = await modlog.fetchMore({ amount: ctx.params.limit || 25 });
            }
        }
        return modlog;
    }
}
exports.default = RedditModlogService;