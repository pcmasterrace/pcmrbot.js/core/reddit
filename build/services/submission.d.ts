import { Context, Service } from "moleculer";
/**
 * This module provides utilities to work with submisisons.
 * All utilities available within `reddit.post` are present in this service.
 *
 * @module "reddit.submission"
 * @version 3
 */
export default class RedditSubmissionService extends Service {
    constructor(broker: any);
    /**
     * Submits a new post to the specified subreddit.
     * @function
     * @static
     * @name submit/create
     * @param {string} subreddit - The subreddit to submit to
     * @param {string} title - The title of the submission
     * @param {string} [text] - The submission text. Optional. Cannot be used with `url`
     * @param {string} [url] - The submission URL. Optional. Cannot be used with `text`
     * @param {boolean} [sendReplies=true] - Whether replies to this submission should be sent to the inbox
     * @returns {object} Returns the thing ID for the new submission
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     * @throws {Errors.MoleculerClientError} Throws an error if both `text` and `url` are supplied
     * @throws Rethrows any errors from Snoowrap
     * @memberof RedditSubmissionService
     */
    submit(ctx: Context): Promise<any>;
    /**
     * Sets the post flair on a specified post.
     * @function
     * @static
     * @name setFlair
     * @param {string} postId - The submission ID to set the flair on
     * @param {string} [text] - The flair text to set (old Reddit only)
     * @param {string} [cssClass] - The CSS class string to set (Old Reddit only). Cannot be used with `templateId`
     * @param {string} [templateId] - The template ID to use (New/Old Reddit). Cannot be used with `cssClass`
     * @returns Returns the Reddit API response, which should be a list of
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    setFlair(ctx: Context): Promise<any>;
    /**
     * Clears the post flair on a specified post.
     * @function
     * @static
     * @name clearFlair
     * @param {string} postId - The submission ID to clear the flair on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    clearFlair(ctx: Context): Promise<boolean>;
    /**
     * Stickies the specified submission.
     * @function
     * @static
     * @name sticky/setAnnouncement
     * @param {string} postId - The submission ID to sticky
     * @param {boolean} [top=false] - Whether the submission should occupy the top or bottom sticky slot
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    sticky(ctx: Context): Promise<boolean>;
    /**
     * Unstickies the specified submission.
     * @function
     * @static
     * @name unsticky/clearAnnouncement
     * @param {string} postId - The submission ID to unsticky
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    unsticky(ctx: Context): Promise<boolean>;
    /**
     * Locks the specified submission.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to lock
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    lock(ctx: Context): Promise<boolean>;
    /**
     * Unlocks the specified submission.
     * @function
     * @static
     * @name unlock
     * @param {string} postId - The submission ID to lock
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    unlock(ctx: Context): Promise<boolean>;
    /**
     * Marks the specified submission as NSFW.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to mark as NSFW
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    markNsfw(ctx: Context): Promise<boolean>;
    /**
     * Unmarks the specified submission as NSFW
     * @function
     * @static
     * @name unmarkNsfw
     * @param {string} postId - The submission ID to unmark as NSFW
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    unmarkNsfw(ctx: Context): Promise<boolean>;
    /**
     * Marks the specified submission as a poiler.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to mark as a spoiler
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    markSpoiler(ctx: Context): Promise<boolean>;
    /**
     * Unmarks the specified submission as a spoiler
     * @function
     * @static
     * @name unmarkSpoiler
     * @param {string} postId - The submission ID to unmark as a spoiler
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    unmarkSpoiler(ctx: Context): Promise<boolean>;
    /**
     * Enables contest mode on the specified submission.
     * @function
     * @static
     * @name enableContestMode
     * @param {string} postId - The submission ID to enable contest mode on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    enableContestMode(ctx: Context): Promise<boolean>;
    /**
     * Disables contest mode for the specified submission
     * @function
     * @static
     * @name disableContestMode
     * @param {string} postId - The submission ID to disable contest mode on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    disableContestMode(ctx: Context): Promise<boolean>;
    /**
     * Sets the suggested sort on a submission.
     * @function
     * @static
     * @name setSuggestedSort
     * @param {string} postId - The submission ID to set the suggested sort on
     * @param {string} sort - One of `confidence`, `top`, `new`, `controversial`, `old`, `random`, `qa`, `live`, or `blank`
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    setSuggestedSort(ctx: Context): Promise<boolean>;
}
