'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
const RedditCoreService = require('./mixin');
/**
 * This module exposes utilities to work with the modqueue
 *
 * @module "reddit.modqueue"
 * @version 3
 */
class ModqueueService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.modqueue',
            version: 3,
            mixins: [RedditCoreService],
            actions: {
                getModqueue: {
                    name: 'get',
                    params: {
                        subreddit: {
                            type: 'string',
                            optional: true
                        },
                        before: {
                            type: 'string',
                            optional: true
                        },
                        after: {
                            type: 'string',
                            optional: true
                        },
                        limit: {
                            type: 'number',
                            integer: true,
                            min: 1,
                            max: 100,
                            optional: true
                        },
                        only: {
                            type: 'enum',
                            values: [
                                'links',
                                'comments'
                            ],
                            optional: true
                        }
                    },
                    handler: this.getModqueue
                }
            }
        });
    }
    /**
     * Retrieves the moderation queue for a subreddit.
     * @function
     * @static
     * @name getModqueue
     * @param {string} [subreddit=mod] - The subreddit to retrieve the modqueue from. Defaults to `/r/mod`, which is all subreddits available to the account
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {string} [only] - The post type to retrieve. One of `links`, `comments`
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     * @returns The modqueue from the specified subreddit(s)
     */
    async getModqueue(ctx) {
        const reddit = ctx.meta.reddit;
        return await reddit.oauthRequest({
            uri: `/r/${ ctx.params.subreddit || 'mod' }/about/modqueue`,
            method: 'get',
            qs: {
                before: ctx.params.before || undefined,
                after: ctx.params.after || undefined,
                only: ctx.params.only || undefined,
                limit: ctx.params.limit || 25
            }
        });
    }
}
exports.default = ModqueueService;