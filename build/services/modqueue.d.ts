import { Service, Context } from "moleculer";
import * as Snoowrap from "snoowrap";
/**
 * This module exposes utilities to work with the modqueue
 *
 * @module "reddit.modqueue"
 * @version 3
 */
export default class ModqueueService extends Service {
    protected reddit: Snoowrap;
    constructor(broker: any);
    /**
     * Retrieves the moderation queue for a subreddit.
     * @function
     * @static
     * @name getModqueue
     * @param {string} [subreddit=mod] - The subreddit to retrieve the modqueue from. Defaults to `/r/mod`, which is all subreddits available to the account
     * @param {string} [before] - The item to retrieve up to. All retrieved actions will be **newer** than this
     * @param {string} [after] - The item to start retrieving from. All retrieved actions will be **older** than this
     * @param {string} [only] - The post type to retrieve. One of `links`, `comments`
     * @param {number} [limit=25] - The number of items to retrieve in one call. Maximum 100
     * @returns The modqueue from the specified subreddit(s)
     */
    getModqueue(ctx: Context): Promise<any>;
}
