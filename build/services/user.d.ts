import { Context, Service } from "moleculer";
/**
 * This module provides utilities to work with users.
 *
 * @module "reddit.user"
 * @version 3
 */
export default class RedditUserService extends Service {
    constructor(broker: any);
    /**
     * Gets the profile of the specified user.
     * @function
     * @static
     * @name get
     * @param {string} user - The user to get
     * @returns {object} Returns the API response
     */
    getUser(ctx: Context): Promise<any>;
    /**
     * Gets the profile of the current active account.
     * @function
     * @static
     * @name getMe
     * @returns {object} Returns the API response
     */
    getMe(ctx: Context): Promise<any>;
    /**
     * Sets the user flair of the specified user in the specified subreddit
     * @function
     * @static
     * @name setFlair
     * @param {string} subreddit - The subreddit to set the flair in
     * @param {string} user - The user to set the flair for
     * @param {string} [text] - The flair text to set
     * @param {string} [cssClass] - The CSS class string to set
     * @returns {boolean} Returns `true` on success
     */
    setFlair(ctx: Context): Promise<boolean>;
    /**
     * Sends a private message to the specified user
     * @function
     * @static
     * @name sendMessage
     * @param {string} to - The user to send the message to
     * @param {string} subject - The subject of the private message
     * @param {string} text - The message text
     * @returns {object} Returns the API response
     */
    sendMessage(ctx: Context): Promise<any>;
    /**
     * Returns the scopes for the account specified in ctx.meta
     * @function
     * @static
     * @name getScopes
     * @returns {string[]} Returns array of strings containing the scopes for the specified refresh token.
     */
    getScopes(ctx: Context): Promise<string[]>;
}
