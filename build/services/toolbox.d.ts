import { Service, Context } from "moleculer";
/**
 * This module exposes several utilities to work with the Moderator Toolbox for Reddit configuration.
 *
 * @module "reddit.toolbox"
 * @version 3
 */
export default class RedditToolboxService extends Service {
    constructor(broker: any);
    /**
     * Retrieves and parses the Toolbox configuration.
     * @function
     * @static
     * @name getConfig
     * @param {string} subreddit - The subreddit from which to retrieve the Toolbox config
     * @returns {object} Returns the Toolbox configuration
     * @throws Throws if Reddit experiences service interruptions
     */
    getConfig(ctx: Context): Promise<any>;
    /**
     * Renders out a removal reason using the Toolbox configuration.
     * @function
     * @static
     * @name renderRemovalMessage
     * @param {string} subreddit - The subreddit to get the removal reasons from. Also used for the `{subreddit}` token
     * @param {string[]} reasons - A string array of removal reasons. This will render out the first rule that the string matches
     * @param {boolean} [useHeader=true] - Whether to use the header for the removal message
     * @param {boolean} [useFooter=true] - Whether to use the footer for the removal message
     * @param {string} [author] - The author of the post being removed. Used for the `{author}` token
     * @param {string} [kind] - The kind of post. Must be `submission` or `comment`. Used for the `{kind}` token
     * @param {string} [mod] - The name of the mod removing the past. Used for the `{mod}` token
     * @param {string} [title] - The title of the post being removed. Used for the `{title}` token
     * @param {string} [url] - The URL to the post being removed. Used for the `{url}` token
     * @param {string} [domain] - The domain of the removed submission. Used for the `{domain}` token
     * @param {string} [link] - The destination link of the removed submission. Used for the `{link}` token
     * @returns {string} The rendered removal reason
     */
    renderRemovalMessage(ctx: Context): Promise<string>;
}
