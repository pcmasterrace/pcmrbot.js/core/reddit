/// <reference types="node" />
import { Context, Service } from "moleculer";
import { Readable } from "stream";
/**
 * This module provides utilities to work with flairs.
 *
 * @module "reddit.flair"
 * @version 3
 */
export default class RedditFlairService extends Service {
    constructor(broker: any);
    getUser(ctx: Context): Promise<import("snoowrap/dist/objects/Subreddit").FlairTemplate>;
    getAll(ctx: Context): Promise<Readable>;
}
