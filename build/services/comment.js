'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// TypeScript complains unless I do this
const RedditPostService = require('./post');
/**
 * This module provides utilities to work with comments.
 * All utilities available within `reddit.post` are present in this service.
 *
 * @module "reddit.comment"
 * @version 3
 */
class RedditCommentService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.comment',
            version: 3,
            mixins: [RedditPostService],
            actions: {
                reply: {
                    name: 'reply',
                    params: {
                        postId: 'string',
                        text: 'string'
                    },
                    handler: this.reply
                },
                sticky: {
                    name: 'sticky',
                    params: { postId: 'string' },
                    handler: this.sticky
                },
                unsticky: {
                    name: 'unsticky',
                    params: {
                        postId: 'string',
                        undistinguish: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.unsticky
                }
            }
        });
    }
    /**
     * Replies to the specified submission or comment.
     * @function
     * @static
     * @name reply
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @param {string} text - The message text to reply with
     * @returns {object} API response from Reddit
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment or a submission
     */
    async reply(ctx) {
        const reddit = ctx.meta.reddit;
        let post;
        if (ctx.params.postId.substr(0, 3) === 't1_') {
            post = reddit.getComment(ctx.params.postId);
        } else if (ctx.params.postId.str(0, 3) === 't3_') {
            post = reddit.getSubmission(ctx.params.postId);
        } else {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
        }
        //@ts-ignore 
        return await post.reply(ctx.params.text);
    }
    /**
     * Distinguishes and stickies the specified comment. This only works for top level comments made by the account being used.
     * @function
     * @static
     * @name sticky
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified does not belong to the account specified
     */
    async sticky(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't1_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a comment', 422, 'INVALID_PARAM_ERROR');
        }
        // TODO: Interpret what Reddit is sending back here and send custom errors as needed
        try {
            // @ts-ignore
            await reddit.getComment(ctx.params.postId).distinguish({ sticky: true });
            return true;
        } catch (err) {
            throw err;
        }
    }
    /**
     * Unstickies the specified comment and optionally undistinguishes it. This only works for top level comments made by the account being used.
     * @function
     * @static
     * @name sticky
     * @param {string} postId - The post ID of the submission/comment to reply to
     * @param {boolean} [undistinguish=true] - Whether the post should also be undistinguished
     * @returns {object} API response from Reddit
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified is not a comment
     * @throws {Errors.MoleculerClientError} Throws an error if the post ID specified does not belong to the account specified
     */
    async unsticky(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't1_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a comment', 422, 'INVALID_PARAM_ERROR');
        }
        // true = distinguish as moderator
        // false = remove distinguish
        // Inverting ctx.params.undistinguish will do what we want here
        let undistinguish = false;
        if (ctx.params.undistinguish !== undefined) {
            undistinguish = !ctx.params.undistinguish;
        }
        // TODO: Interpret what Reddit is sending back here and send custom errors as needed
        try {
            // @ts-ignore
            await reddit.getComment(ctx.params.postId).distinguish({
                status: undistinguish,
                sticky: false
            });
            return true;
        } catch (err) {
            throw err;
        }
    }
}
exports.default = RedditCommentService;