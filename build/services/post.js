'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// TypeScript complains unless I do this
const RedditCoreService = require('./mixin');
// This module has to be a ServiceSchema instead of an ES6 class because Moleculer's mixin feature won't work otherwise.
/**
 * This module provides utilities to work with comments and submissions.
 *
 * @module "reddit.post"
 * @version 3
 */
module.exports = {
    name: 'reddit.post',
    version: 3,
    mixins: [RedditCoreService],
    actions: {
        get: {
            name: 'get',
            params: { postId: 'string' },
            /**
             * Gets a comment or a submission.
             * @function
             * @static
             * @name get
             * @param {string} postId - The post to get
             * @returns {object} Returns the API response
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    return await post.fetch();
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        edit: {
            name: 'edit',
            params: {
                postId: 'string',
                text: 'string'
            },
            /**
             * Edits the text of a comment or a submission.
             * @function
             * @static
             * @name edit
             * @param {string} postId - The post to edit
             * @param {string} text - The replacement post text
             * @returns {object} Returns the API response
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    return await post.edit(ctx.params.text);
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        delete: {
            name: 'delete',
            params: { postId: 'string' },
            /**
             * Deletes a comment or a submission.
             * @function
             * @static
             * @name delete
             * @param {string} postId - The post to delete
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.delete();
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        report: {
            name: 'report',
            params: {
                postId: 'string',
                reason: {
                    type: 'string',
                    optional: true
                }
            },
            /**
             * Reports a comment or a submission.
             * @function
             * @static
             * @name report
             * @param {string} postId - The post to report
             * @param {string} [reason] - The replacement post text
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.report({ reason: ctx.params.reason });
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        approve: {
            name: 'approve',
            params: { postId: 'string' },
            /**
             * Approves a comment or a submission.
             * @function
             * @static
             * @name approve
             * @param {string} postId - The post to approve
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.approve();
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        remove: {
            name: 'remove',
            params: {
                postId: 'string',
                spam: {
                    type: 'boolean',
                    optional: true
                }
            },
            /**
             * Removes a comment or a submission.
             * @function
             * @static
             * @name remove
             * @param {string} postId - The post to remove
             * @param {boolean} [spam=false] - Whether the post should be marked as spam
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.remove({ spam: ctx.params.spam });
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        ignore: {
            name: 'ignore',
            params: { postId: 'string' },
            /**
             * Ignores reports on a comment or a submission.
             * @function
             * @static
             * @name ignore
             * @param {string} postId - The post to ignore
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.ignoreReports();
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        unignore: {
            name: 'unignore',
            params: { postId: 'string' },
            /**
             * Unignores reports on a comment or a submission.
             * @function
             * @static
             * @name unignore
             * @param {string} postId - The post to unignore
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.unignoreReports();
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        distinguish: {
            name: 'distinguish',
            params: { postId: 'string' },
            /**
             * Distinguishes a comment or a submission.
             * @function
             * @static
             * @name distinguish
             * @param {string} postId - The post to distinguish
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.distinguish({ status: true });
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        },
        undistinguish: {
            name: 'undistinguish',
            params: { postId: 'string' },
            /**
             * Undistinguishes a comment or a submission.
             * @function
             * @static
             * @name undistinguish
             * @param {string} postId - The post to undistinguish
             * @returns {boolean} Returns `true` on success
             * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a comment or a submission
             */
            async handler(ctx) {
                const reddit = ctx.meta.reddit;
                let post;
                if (ctx.params.postId.substr(0, 3) === 't1_') {
                    post = reddit.getComment(ctx.params.postId);
                } else if (ctx.params.postId.substr(0, 3) === 't3_') {
                    post = reddit.getSubmission(ctx.params.postId);
                } else {
                    throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID not a comment or submission', 422, 'INVALID_PARAM_ERROR');
                }
                try {
                    // @ts-ignore
                    await post.distinguish({ status: false });
                    return true;
                } catch (err) {
                    // TODO: Handle Reddit/Snoowrap errors
                    throw err;
                }
            }
        }
    }
};