'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// TypeScript complains unless I do this
const RedditCoreService = require('./mixin');
/**
 * This module provides utilities to work with users.
 *
 * @module "reddit.user"
 * @version 3
 */
class RedditUserService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.user',
            version: 3,
            mixins: [RedditCoreService],
            actions: {
                get: {
                    name: 'get',
                    params: { user: 'string' },
                    handler: this.getUser
                },
                getMe: {
                    name: 'getMe',
                    handler: this.getMe
                },
                setFlair: {
                    name: 'setFlair',
                    params: {
                        subreddit: 'string',
                        user: 'string',
                        text: {
                            type: 'string',
                            optional: true
                        },
                        cssClass: {
                            type: 'string',
                            optional: true
                        }
                    },
                    handler: this.setFlair
                },
                clearFlair: {
                    name: 'clearFlair',
                    params: {
                        subreddit: 'string',
                        user: 'string'
                    },
                    handler: this.setFlair
                },
                getScopes: {
                    name: 'getScopes',
                    handler: this.getScopes
                },
                sendMessage: {
                    name: 'sendMessage',
                    params: {
                        subject: {
                            type: 'string',
                            empty: false
                        },
                        text: {
                            type: 'string',
                            empty: false
                        },
                        to: {
                            type: 'string',
                            empty: false
                        }
                    },
                    handler: this.sendMessage
                }
            }
        });
    }
    /**
     * Gets the profile of the specified user.
     * @function
     * @static
     * @name get
     * @param {string} user - The user to get
     * @returns {object} Returns the API response
     */
    async getUser(ctx) {
        const reddit = ctx.meta.reddit;
        try {
            // @ts-ignore
            return await reddit.getUser(ctx.params.user).fetch();
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Gets the profile of the current active account.
     * @function
     * @static
     * @name getMe
     * @returns {object} Returns the API response
     */
    async getMe(ctx) {
        const reddit = ctx.meta.reddit;
        try {
            // @ts-ignore
            return await reddit.getMe();
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Sets the user flair of the specified user in the specified subreddit
     * @function
     * @static
     * @name setFlair
     * @param {string} subreddit - The subreddit to set the flair in
     * @param {string} user - The user to set the flair for
     * @param {string} [text] - The flair text to set
     * @param {string} [cssClass] - The CSS class string to set
     * @returns {boolean} Returns `true` on success
     */
    async setFlair(ctx) {
        const reddit = ctx.meta.reddit;
        try {
            // @ts-ignore
            await reddit.getUser(ctx.params.user).assignFlair({
                subreddit: ctx.params.subreddit,
                text: ctx.params.text,
                cssClass: ctx.params.cssClass
            });
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Sends a private message to the specified user
     * @function
     * @static
     * @name sendMessage
     * @param {string} to - The user to send the message to
     * @param {string} subject - The subject of the private message
     * @param {string} text - The message text
     * @returns {object} Returns the API response
     */
    async sendMessage(ctx) {
        const reddit = ctx.meta.reddit;
        try {
            return await reddit.composeMessage({
                to: ctx.params.to,
                subject: ctx.params.subject,
                text: ctx.params.text
            });
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Returns the scopes for the account specified in ctx.meta
     * @function
     * @static
     * @name getScopes
     * @returns {string[]} Returns array of strings containing the scopes for the specified refresh token.
     */
    async getScopes(ctx) {
        const reddit = ctx.meta.reddit;
        return reddit.scope;
    }
}
exports.default = RedditUserService;