'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
// TypeScript complains unless I do this
const RedditPostService = require('./post');
/**
 * This module provides utilities to work with submisisons.
 * All utilities available within `reddit.post` are present in this service.
 *
 * @module "reddit.submission"
 * @version 3
 */
class RedditSubmissionService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'reddit.submission',
            version: 3,
            mixins: [RedditPostService],
            actions: {
                create: {
                    name: 'create',
                    params: {
                        subreddit: 'string',
                        title: 'string',
                        text: {
                            type: 'string',
                            optional: true
                        },
                        url: {
                            type: 'url',
                            optional: true
                        },
                        sendReplies: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.submit
                },
                submit: {
                    name: 'submit',
                    params: {
                        subreddit: 'string',
                        title: 'string',
                        text: {
                            type: 'string',
                            optional: true
                        },
                        url: {
                            type: 'string',
                            optional: true
                        },
                        sendReplies: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.submit
                },
                setFlair: {
                    name: 'setFlair',
                    params: {
                        postId: 'string',
                        text: {
                            type: 'string',
                            optional: true
                        },
                        cssClass: {
                            type: 'string',
                            optional: true
                        },
                        templateId: {
                            type: 'string',
                            optional: true
                        }
                    },
                    handler: this.setFlair
                },
                clearFlair: {
                    name: 'clearFlair',
                    params: { postId: 'string' },
                    handler: this.clearFlair
                },
                sticky: {
                    name: 'sticky',
                    params: {
                        postId: 'string',
                        top: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.sticky
                },
                unsticky: {
                    name: 'unsticky',
                    params: { postId: 'string' },
                    handler: this.unsticky
                },
                setAnnouncement: {
                    name: 'setAnnouncement',
                    params: {
                        postId: 'string',
                        top: {
                            type: 'boolean',
                            optional: true
                        }
                    },
                    handler: this.sticky
                },
                clearAnnouncement: {
                    name: 'clearAnnouncement',
                    params: { postId: 'string' },
                    handler: this.unsticky
                },
                lock: {
                    name: 'lock',
                    params: { postId: 'string' },
                    handler: this.lock
                },
                unlock: {
                    name: 'unlock',
                    params: { postId: 'string' },
                    handler: this.unlock
                },
                markNsfw: {
                    name: 'markNsfw',
                    params: { postId: 'string' },
                    handler: this.markNsfw
                },
                unmarkNsfw: {
                    name: 'unmarkNsfw',
                    params: { postId: 'string' },
                    handler: this.unmarkNsfw
                },
                markSpoiler: {
                    name: 'markSpoiler',
                    params: { postId: 'string' },
                    handler: this.markSpoiler
                },
                unmarkSpoiler: {
                    name: 'unmarkSpoiler',
                    params: { postId: 'string' },
                    handler: this.unmarkSpoiler
                },
                enableContestMode: {
                    name: 'enableContestMode',
                    params: { postId: 'string' },
                    handler: this.enableContestMode
                },
                disableContestMode: {
                    name: 'disableContestMode',
                    params: { postId: 'string' },
                    handler: this.disableContestMode
                },
                setSuggestedSort: {
                    name: 'setSuggestedSort',
                    params: {
                        postId: 'string',
                        sort: 'string'
                    },
                    handler: this.setSuggestedSort
                }
            }
        });
    }
    /**
     * Submits a new post to the specified subreddit.
     * @function
     * @static
     * @name submit/create
     * @param {string} subreddit - The subreddit to submit to
     * @param {string} title - The title of the submission
     * @param {string} [text] - The submission text. Optional. Cannot be used with `url`
     * @param {string} [url] - The submission URL. Optional. Cannot be used with `text`
     * @param {boolean} [sendReplies=true] - Whether replies to this submission should be sent to the inbox
     * @returns {object} Returns the thing ID for the new submission
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     * @throws {Errors.MoleculerClientError} Throws an error if both `text` and `url` are supplied
     * @throws Rethrows any errors from Snoowrap
     * @memberof RedditSubmissionService
     */
    async submit(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.text !== undefined && ctx.params.url !== undefined) {
            throw new moleculer_1.Errors.MoleculerClientError('Cannot supply both a url and text', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            if (ctx.params.text !== undefined || ctx.params.text === undefined && ctx.params.url === undefined) {
                // @ts-ignore
                return await reddit.submitSelfpost({
                    subredditName: ctx.params.subreddit,
                    title: ctx.params.title,
                    text: ctx.params.text,
                    sendReplies: ctx.params.sendReplies !== undefined ? ctx.params.sendReplies : true
                });
            } else {
                // This interface is pretty incomplete. Needs fixing upstream				
                return await reddit.submitLink({
                    // @ts-ignore
                    subredditName: ctx.params.subreddit,
                    title: ctx.params.title,
                    url: ctx.params.url,
                    // @ts-ignore
                    sendReplies: ctx.params.sendReplies !== undefined ? ctx.params.sendReplies : true,
                    resubmit: true
                });
            }
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Sets the post flair on a specified post.
     * @function
     * @static
     * @name setFlair
     * @param {string} postId - The submission ID to set the flair on
     * @param {string} [text] - The flair text to set (old Reddit only)
     * @param {string} [cssClass] - The CSS class string to set (Old Reddit only). Cannot be used with `templateId`
     * @param {string} [templateId] - The template ID to use (New/Old Reddit). Cannot be used with `cssClass`
     * @returns Returns the Reddit API response, which should be a list of
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async setFlair(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            return await reddit.oauthRequest({
                uri: '/api/selectflair',
                method: 'post',
                form: {
                    api_type: 'json',
                    css_class: ctx.params.cssClass || undefined,
                    flair_template_id: ctx.params.templateId || undefined,
                    link: ctx.params.postId,
                    text: ctx.params.text || undefined
                }
            });
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Clears the post flair on a specified post.
     * @function
     * @static
     * @name clearFlair
     * @param {string} postId - The submission ID to clear the flair on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async clearFlair(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).assignFlair({
                text: '',
                cssClass: ''
            });
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Stickies the specified submission.
     * @function
     * @static
     * @name sticky/setAnnouncement
     * @param {string} postId - The submission ID to sticky
     * @param {boolean} [top=false] - Whether the submission should occupy the top or bottom sticky slot
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async sticky(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).sticky({ num: ctx.params.top ? 1 : 2 });
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Unstickies the specified submission.
     * @function
     * @static
     * @name unsticky/clearAnnouncement
     * @param {string} postId - The submission ID to unsticky
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async unsticky(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).unsticky();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Locks the specified submission.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to lock
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async lock(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).lock();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Unlocks the specified submission.
     * @function
     * @static
     * @name unlock
     * @param {string} postId - The submission ID to lock
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async unlock(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).unlock();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Marks the specified submission as NSFW.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to mark as NSFW
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async markNsfw(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).markNsfw();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Unmarks the specified submission as NSFW
     * @function
     * @static
     * @name unmarkNsfw
     * @param {string} postId - The submission ID to unmark as NSFW
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async unmarkNsfw(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).unmarkNsfw();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Marks the specified submission as a poiler.
     * @function
     * @static
     * @name lock
     * @param {string} postId - The submission ID to mark as a spoiler
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async markSpoiler(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).markSpoiler();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Unmarks the specified submission as a spoiler
     * @function
     * @static
     * @name unmarkSpoiler
     * @param {string} postId - The submission ID to unmark as a spoiler
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async unmarkSpoiler(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).unmarkSpoiler();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Enables contest mode on the specified submission.
     * @function
     * @static
     * @name enableContestMode
     * @param {string} postId - The submission ID to enable contest mode on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async enableContestMode(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).enableContestMode();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Disables contest mode for the specified submission
     * @function
     * @static
     * @name disableContestMode
     * @param {string} postId - The submission ID to disable contest mode on
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async disableContestMode(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).disableContestMode();
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
    /**
     * Sets the suggested sort on a submission.
     * @function
     * @static
     * @name setSuggestedSort
     * @param {string} postId - The submission ID to set the suggested sort on
     * @param {string} sort - One of `confidence`, `top`, `new`, `controversial`, `old`, `random`, `qa`, `live`, or `blank`
     * @returns {boolean} Returns `true` on success
     * @throws {Errors.MoleculerClientError} Throws an error if the supplied post ID is not a submission
     */
    async setSuggestedSort(ctx) {
        const reddit = ctx.meta.reddit;
        if (ctx.params.postId.substr(0, 3) !== 't3_') {
            throw new moleculer_1.Errors.MoleculerClientError('Supplied post ID is not a submission', 422, 'INVALID_PARAM_ERROR');
        }
        try {
            // @ts-ignore
            await reddit.getSubmission(ctx.params.postId).setSuggestedSort(ctx.params.sort);
            return true;
        } catch (err) {
            // TODO: Handle Reddit/Snoowrap errors
            throw err;
        }
    }
}
exports.default = RedditSubmissionService;