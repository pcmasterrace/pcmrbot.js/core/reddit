import { Context, Service, Errors } from "moleculer";
import * as Snoowrap from "snoowrap";
import * as MultiStream from "multistream"; 

import { Readable, pipeline } from "stream";

// TypeScript complains unless I do this
const RedditCoreService = require("@services/mixin");

/**
 * This module provides utilities to work with flairs.
 * 
 * @module "reddit.flair"
 * @version 3
 */
export default class RedditFlairService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "reddit.flair",
			version: 3,
			mixins: [RedditCoreService],
			actions: {
				getUser: {
					name: "get.user",
					params: {
						subreddit: "string",
						user: "string"
					},
					handler: this.getUser
				},
				getAll: {
					name: "get.all",
					params: {
						subreddit: "string",
						limit: {
							type: "number",
							optional: true,
							integer: true,
							positive: true,
							max: 1000
						},
						after: {
							type: "string",
							optional: true
						},
						//stream: "any"
					},
					handler: this.getAll
				}
			}
		});
	}

	async getUser(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		try {
			return await reddit.getSubreddit(ctx.params.subreddit).getUserFlair(ctx.params.user);
		} catch (err) {
			throw new Errors.MoleculerError("Snoowrap error", err.code, "CHANGE_THIS", err);
		}
	}

	async getAll(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;
		
		// The caller should have supplied a stream
		const stream = new Readable({
			read() {}
		});
		let flairCount = 0;

		// Do it once first outside of the loop so the typing is correctly set, also because the params are different 
		reddit
		.getSubreddit(ctx.params.subreddit)
		.getUserFlairList({limit: 1000})
		.then(async listing => {
			for (let flair of listing) {
				flairCount++;
				stream.push(JSON.stringify({
					flair_css_class: flair.flair_css_class,
					//@ts-ignore (this is a typings bug)
					name: flair.user.name,
					flair_text: flair.flair_text
				}));
			}

			while (!listing.isFinished) {
				listing = await listing.fetchMore({amount: 1000, append: false});
				for (let flair of listing) {
					flairCount++;
					stream.push(JSON.stringify({
						flair_css_class: flair.flair_css_class,
						//@ts-ignore (this is a typings bug)
						name: flair.user.name,
						flair_text: flair.flair_text
					}));
				}
			}
		})
		.then(() => {
			// End stream
			stream.push(null);
		});

		return stream;
	}
}