import { Context, Service, Errors } from "moleculer";
import * as Snoowrap from "snoowrap";
import * as moment from "moment-timezone";

const RedditCoreService = require("@services/mixin");

/**
 * This module exposes various utilities to work with New Modmail
 * 
 * @module "reddit.modmail"
 * @version 3
 */
export default class ModmailService extends Service {
	protected reddit: Snoowrap;

	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "reddit.modmail",
			version: 3,
			mixins: [RedditCoreService],
			actions: {
				getConversation: {
					name: "getConversation",
					params: {
						conversationId: "string"
					},
					handler: this.getConversation
				},
				getConversations: {
					name: "getConversations",
					params: {
						after: {type: "string", optional: true},
						entity: {type: "string", optional: true},
						limit: {type: "number", optional: true},
						sort: {type: "string", optional: true},
						state: {type: "string", optional: true}
					},
					handler: this.getConversations
				},
				createNewConversation: {
					name: "createNewConversation",
					params: {
						body: "string",
						isAuthorHidden: "boolean",
						srName: "string",
						subject: "string",
						to: "string",
					},
					handler: this.createNewConversation
				},
				postNewReply: {
					name: "postNewReply",
					params: {
						conversationId: "string",
						body: "string",
						isAuthorHidden: {type: "boolean", optional: true},
						isInternal: {type: "boolean", optional: true},
					},
					handler: this.postNewReply
				},
				archive: {
					name: "archive",
					params: {
						conversationId: "string",
					},
					handler: this.archive
				},
				unarchive: {
					name: "unarchive",
					params: {
						conversationId: "string",
					},
					handler: this.unarchive
				},
				highlight: {
					name: "highlight",
					params: {
						conversationId: "string",
					},
					handler: this.highlight
				},
				unhighlight: {
					name: "unhighlight",
					params: {
						conversationId: "string",
					},
					handler: this.unhighlight
				},
				mute: {
					name: "mute",
					params: {
						conversationId: "string",
					},
					handler: this.mute
				},
				unmute: {
					name: "unmute",
					params: {
						conversationId: "string",
					},
					handler: this.unmute
				},
				markRead: {
					name: "markRead",
					params: {
						conversationId: {
							type: "array", items: "string"
						},
					},
					handler: this.markRead
				},
				markUnread: {
					name: "markUnread",
					params: {
						conversationId: {
							type: "array", items: "string"
						}
					},
					handler: this.markUnread
				},
				getUnreadCount: {
					name: "getUnreadCount",
					handler: this.getUnreadCount
				},
				getSubreddits: {
					name: "getSubreddits",
					handler: this.getSubreddits
				}
			}
		});
	}

	/**
	 * Retrieves all of the messages and stuff in a conversation.
	 * @function
	 * @static
	 * @name getConversation
	 * @param {string} conversationId - The conversation ID.
	 * @returns {object} The conversation as returned by Reddit. 
	 */
	async getConversation(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;
		// Because Request lacks a URI option, all direct OAuth calls need to 
		// declare the options object as type any so the compiler won't yell at me.
		let raw = await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}`,
			method: "get"
		} as any);

		// @ts-ignore
		let conversation: ModmailConversation = {};
		conversation.conversation = [];
		// @ts-ignore
		conversation.info = {};

		for (let i = 0; i < raw.conversation.objIds.length; i++) {
			let objId = raw.conversation.objIds[i];

			// Reformat the conversation to be a bit more linear
			// @ts-ignore
			let action: ModmailConversationEntry = {};
			
			action.id = objId.id;

			let id = objId.id;

			// @ts-ignore
			action.author = {};
			
			if (objId.key === "messages") {
				action.type = "message";
				action.date = moment(raw.messages[id].date).toISOString();
				action.isInternal = raw.messages[id].isInternal;
				action.message = raw.messages[id].bodyMarkdown;
				
				action.author.id = raw.messages[id].author.name.id;
				action.author.name = raw.messages[id].author.name.name;
				action.author.isMod = raw.messages[id].author.name.isMod;
				action.author.isAdmin = raw.messages[id].author.name.isAdmin;
				action.author.isOp = raw.messages[id].author.name.isOp;
				action.author.isHidden = raw.messages[id].author.name.isHidden;

				// More information if this is the OP
				if(i === 0) {
					action.author.created = moment(raw.user.name.created).toISOString();

					action.author.isSuspended = raw.user.name.isSuspended;
					action.author.isShadowbanned = raw.user.name.isShadowbanned;

					// @ts-ignore
					action.author.banStatus = {};
					action.author.banStatus.banReason = raw.user.name.banStatus.reason;
					action.author.banStatus.expiresOn = moment(raw.user.name.banStatus.endDate).toISOString();
					action.author.banStatus.isBanned = raw.user.name.banStatus.isBanned;
					action.author.banStatus.isPermanent = raw.user.name.banStatus.isPermanent;
					
					// @ts-ignore
					action.author.muteStatus = {};
					action.author.muteStatus.muteReason = raw.user.name.muteStatus.reason;
					action.author.muteStatus.expiresOn = moment(raw.user.name.muteStatus.endDate).toISOString();
					action.author.muteStatus.isMuted = raw.user.name.muteStatus.isMuted;
					
					action.author.recent = {};
					if (raw.user.name.recentComments !== undefined) {
						action.author.recent.comments = {};

						for (let x in raw.user.name.recentComments) {
							// @ts-ignore
							action.author.recent.comments[x] = {};
							action.author.recent.comments[x].comment = raw.user.name.recentComments[x].comment;
							action.author.recent.comments[x].date = moment(raw.user.name.recentComments[x].date).toISOString();
							action.author.recent.comments[x].permalink = raw.user.name.recentComments[x].permalink;
							action.author.recent.comments[x].title = raw.user.name.recentComments[x].title;
						}
					}
					
					if (raw.user.name.recentPosts !== undefined) {
						action.author.recent.posts = {};

						for (let x in raw.user.name.recentPosts) {
							// @ts-ignore
							action.author.recent.posts[x] = {};
							action.author.recent.posts[x].date = moment(raw.user.name.recentPosts[x].date).toISOString();
							action.author.recent.posts[x].permalink = raw.user.name.recentPosts[x].permalink;
							action.author.recent.posts[x].title = raw.user.name.recentPosts[x].title;
						}
					}
					
					if (raw.user.name.recentConvos !== undefined) {
						action.author.recent.conversations = {};

						for (let x in raw.user.name.recentConvos) {
							// @ts-ignore
							action.author.recent.conversations[x] = {};
							action.author.recent.conversations[x].date = moment(raw.user.name.recentConvos[x].date).toISOString();
							action.author.recent.conversations[x].id = raw.user.name.recentConvos[x].id;
							action.author.recent.conversations[x].subject = raw.user.name.recentConvos[x].subject;
						}
					}
				}
			} else if (objId.key === "modActions") {
				action.type = "modAction";
				action.actionTypeId = raw.modActions[id].actionTypeId;
				action.date = moment(raw.modActions[id].date).toISOString();

				action.author.id = raw.modActions[id].author.name.id;
				action.author.name = raw.modActions[id].author.name.name;
				action.author.isMod = raw.modActions[id].author.name.isMod;
				action.author.isAdmin = raw.modActions[id].author.name.isAdmin;
				action.author.isOp = raw.modActions[id].author.name.isOp;
				action.author.isHidden = raw.modActions[id].author.name.isHidden;
			}

			// Now that we've formatted the action, let's add it to the larger conversation
			conversation.conversation.push(action);
		}

		// And now that we've got our actions, let's get the info set 
		conversation.info.id = raw.conversation.id;
		conversation.info.isHighlighted = raw.conversation.isHighlighted;
		conversation.info.subject = raw.conversation.subject;
		conversation.info.subreddit = raw.conversation.owner.displayName;
		conversation.info.subredditId = raw.conversation.owner.id;
		conversation.info.isAuto = raw.conversation.isAuto; 

		// And there we go! Let's now return this. 
		return conversation;
	}

	/**
	 * Retrieve the current list of modmails. 
	 * @function
	 * @static
	 * @name getConversations
	 * @param {string} [after=new] - Retrieves modmails older than the supplied ID
	 * @param {string} [entity=all] - Comma-delimited list of subreddit names
	 * @param {number} [limit=25] - Number of modmails to retrieve
	 * @param {string} [sort=new] - One of the following: `recent`, `new`, `mod`, `unread`
	 * @param {string} [state=all] - One of the following: `new`, `inprogress`, `mod`, `notifications`, `archived`, `highlighted`, `all`
	 * @returns {object} The results of the API request.
	 */
	async getConversations(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		// TODO: Implement page iteration functionality
		return await reddit.oauthRequest({
			uri: "/api/mod/conversations", 
			method: "get", 
			qs: {
				after: ctx.params.after || undefined,
				entity: ctx.params.entity || undefined,
				limit: ctx.params.limit || undefined,
				sort: ctx.params.sort || "recent", 
				state: ctx.params.state || "all"
			}
		} as any);
	}
	
	/**
	 * Creates a new modmail conversation.
	 * @function
	 * @static
	 * @name createNewConversation
	 * @param {string} body - The Markdown body of the message
	 * @param {boolean} isAuthorHidden - Whether the message should be sent as a user or as the subreddit
	 * @param {string} srName - The subreddit from which to send the message
	 * @param {string} subject - The message subject
	 * @param {string} to - The recipient of the message
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API response, probably the base36 ID of the conversation.
	 */
	async createNewConversation(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: "/api/mod/conversations",
			method: "post",
			form: {
				body: ctx.params.body,
				isAuthorHidden: ctx.params.isAuthorHidden || false,
				srName: ctx.params.srName,
				subject: ctx.params.subject,
				to: ctx.params.to
			}
		} as any);
	}
	
	/**
	 * Posts a new message to a specified modmail conversation. 
	 * @function
	 * @static
	 * @name postNewReply
	 * @param {string} conversationId - The base36 conversation ID
	 * @param {string} body - The Markdown body of the message
	 * @param {boolean} [isAuthorHidden=false] - Boolean representing whether message should be sent as a user or as subreddit
	 * @param {boolean} [isInternal=false] - Boolean representing whether the message is an internal message
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API response to this.
	 */
	async postNewReply(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}`,
			method: "post",
			form: {
				body: ctx.params.body,
				isAuthorHidden: ctx.params.isAuthorHidden || false,
				isInternal: ctx.params.isInternal || false
			}
		} as any);
	}
	
	/**
	 * Archives a modmail conversation
	 * @function
	 * @static
	 * @name archive
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async archive(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/archive`,
			method: "post"
		} as any);
	}
	
	/**
	 * Unarchives a modmail conversation
	 * @function
	 * @static
	 * @name unarchive
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async unarchive(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/unarchive`,
			method: "post"
		} as any);
	}
	
	/**
	 * Highlights a modmail conversation
	 * @function
	 * @static
	 * @name highlight
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async highlight(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/highlight`,
			method: "post"
		} as any);
	}
	
	/**
	 * Removes highlight from a modmail conversation
	 * @function
	 * @static
	 * @name unhighlight
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async unhighlight(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/highlight`,
			method: "delete"
		} as any);
	}
	
	/**
	 * Mutes a modmail conversation
	 * @function
	 * @static
	 * @name mute
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async mute(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/mute`,
			method: "post"
		} as any);
	}
	
	/**
	 * Unmutes a modmail conversation
	 * @function
	 * @static
	 * @name unmute
	 * @param {string} conversationId - The base36 ID of the specified conversation
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async unmute(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/${ctx.params.conversationId}/unmute`,
			method: "post"
		} as any);
	}

	/**
	 * Marks a modmail conversation, or a set of modmail conversations, read.
	 * @function
	 * @static
	 * @name markRead
	 * @param {string} conversationIds - A comma separated list of base36 conversation IDs
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async markRead(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/read`,
			method: "post",
			form: {
				conversationIds: ctx.params.conversationIds.join(",")
			}
		} as any);
	}

	/**
	 * Marks a modmail conversation, or a set of modmail conversations, unread.
	 * 
	 * @function
	 * @static
	 * @name markUnread
	 * @param {string} conversationIds - A comma separated list of base36 conversation IDs
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async markUnread(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/unread`,
			method: "post",
			form: {
				conversationIds: ctx.params.conversationIds.join(",")
			}
		} as any);
	}
	
	/**
	 * Gets the current unread modmail count.
	 * @function
	 * @static
	 * @name getUnreadCount
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async getUnreadCount(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/unread/count`,
			method: "get"
		} as any);
	}
	
	/**
	 * Gets a list of subreddits that the current authenticated user has access to
	 * @function
	 * @static
	 * @name getSubreddits
	 * @param {string} [redditName=Default user] - The username of the user to act as
	 * @param {string} [slackId=Default user] - The slack ID of the user to act as
	 * @returns {object} The API results.
	 */
	async getSubreddits(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		return await reddit.oauthRequest({
			uri: `/api/mod/conversations/subreddits`,
			method: "get"
		} as any);
	}
}

// These interfaces are mostly for sanity checking, as Snoowrap has no utilities to work with modmail
interface ModmailConversation {
	conversation: ModmailConversationEntry[],
	info: {
		id: string,
		subreddit: string,
		subredditId: string,
		isAuto: boolean,
		isHighlighted: boolean,
		subject: string
	}
}

interface ModmailConversationEntry {
	type: "message" | "modAction",
	id: string,
	author: {
		name: string,
		id: string | number,
		isOp: boolean,
		isMod: boolean,
		isAdmin: boolean,
		isHidden: boolean,
		created?: string,
		muteStatus?: {
			isMuted: boolean,
			expiresOn: string,
			muteReason: string
		},
		banStatus?: {
			isBanned: boolean,
			isPermanent: boolean,
			expiresOn: string,
			banReason: string
		},
		isSuspended?: boolean,
		isShadowbanned?: boolean,
		recent?: {
			comments?: {
				[name: string]: {
					comment: string,
					date: string,
					permalink: string,
					title: string
				}
			},
			posts?: {
				[name: string]: {
					date: string,
					permalink: string,
					title: string
				}
			},
			conversations?: {
				[name: string]: {
					date: string,
					id: string,
					subject: string
				}
			}
		}
	},
	date: string,
	isInternal ?: boolean,
	message ?: string,
	actionTypeId ?: number
}
