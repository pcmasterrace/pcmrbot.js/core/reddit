import { Service, Context, Errors } from "moleculer";
import * as Snoowrap from "snoowrap";

// TypeScript complains unless I do this
const RedditCoreService = require("@services/mixin");

/**
 * This module exposes several utilities to work with subreddit wikis.
 *
 * @module "reddit.wiki"
 * @version 3
 */
export default class RedditWikiService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "reddit.wiki",
			version: 3,
			mixins: [RedditCoreService],
			actions: {
				getPage: {
					name: "getPage",
					params: {
						subreddit: "string",
						page: "string"
					},
					handler: this.getPage
				},
				setPage: {
					name: "setPage",
					params: {
						subreddit: "string",
						page: "string",
						text: "string",
						reason: {
							type: "string",
							optional: true
						}
					},
					handler: this.setPage
				},
				createPage: {
					name: "createPage",
					params: {
						subreddit: "string",
						page: "string",
						text: "string",
						reason: {
							type: "string",
							optional: true
						}
					},
					handler: this.setPage
				}
			}
		});
	}

	/**
	 * Retrieves a wiki page from the specified subreddit
	 * @function
	 * @static
	 * @name getPage
	 * @param {string} subreddit - The subreddit to retrieve the page from
	 * @param {string} page - The wiki page to retrieve
	 * @returns {object} The API response from Reddit
	 * @throws {Errors.MoleculerError} Throws an error if Reddit encounters any service related issues
	 */
	async getPage(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;

		// @ts-ignore
		return await reddit.getSubreddit(ctx.params.subreddit).getWikiPage(ctx.params.page).fetch();
	}
	
	/**
	 * Creates or updates the content of a wiki page
	 * @function
	 * @static
	 * @name setPage/createPage
	 * @param {string} subreddit - The subreddit to edit the page on
	 * @param {string} page - The wiki page to edit/create
	 * @param {string} text - The updated contents of the wiki page
	 * @param {string} [reason] - The edit reason for the page
	 * @returns {object} The API response from Reddit
	 * @throws {Errors.MoleculerError} Throws an error if Reddit encounters any service related issues
	 */
	async setPage(ctx: Context) {
		const reddit: Snoowrap = ctx.meta.reddit;


		// @ts-ignore
		return await reddit.getSubreddit(ctx.params.subreddit)
			.getWikiPage(ctx.params.page)
			.edit({
				text: ctx.params.text,
				reason: ctx.params.reason || undefined
			});
	}
}