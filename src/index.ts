import { ServiceBroker, ServiceSchema } from "moleculer";

// Because this is a ServiceSchema instead of a regular ES6 class, I have to use require() instead
const RedditPostService: ServiceSchema = require("@services/post");
import RedditCommentService from "@services/comment";
import RedditFlairService from "@services/flair";
import RedditModlogService from "@services/modlog";
import RedditModmailService from "@services/modmail";
import RedditModqueueService from "@services/modqueue";
import RedditSubmissionService from "@services/submission";
import RedditSubredditService from "@services/subreddit";
import RedditToolboxService from "@services/toolbox";
import RedditUserService from "@services/user";
import RedditWikiService from "@services/wiki";

/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker 
 */
export default function registerAllRedditServices(broker: ServiceBroker): void {
	broker.createService(RedditPostService);
	broker.createService(RedditCommentService);
	broker.createService(RedditFlairService);
	broker.createService(RedditModlogService);
	broker.createService(RedditModmailService);
	broker.createService(RedditModqueueService);
	broker.createService(RedditSubmissionService);
	broker.createService(RedditSubredditService);
	broker.createService(RedditToolboxService);
	broker.createService(RedditUserService);
	broker.createService(RedditWikiService);
}

export {
	RedditPostService,
	RedditCommentService, 
	RedditFlairService,
	RedditModlogService,
	RedditModmailService,
	RedditModqueueService,
	RedditSubmissionService,
	RedditSubredditService,
	RedditToolboxService,
	RedditUserService,
	RedditWikiService
}